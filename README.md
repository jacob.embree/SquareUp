SquareUp
======
Play the game of Square Up! on your computer.

A PyGTK implementation of Square Up!.

##Installation
Open a terminal and enter the following command:

    wget https://raw.githubusercontent.com/ralphembree/SquareUp/master/install.sh -q -O - | bash

This will put the executable file in /usr/games.  To run the program, simply type squareup into the command prompt or click Square Up in your Games menu.

##Features
* Grid size can be determined by the user.
* Flashy colors can be reset (even in the middle of a game).
* Highscores are saved.

##Screenshot
<p align="center">
  <img src="https://github.com/ralphembree/SquareUp/blob/master/screenshot.png" alt="Screenshot"/>
</p>

## License
SquareUp is a Python implementation of the board game.
Copyright © 2016 Ralph Embree

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILLITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see [http://www.gnu.org/licenses](http://www.gnu.org/licenses).


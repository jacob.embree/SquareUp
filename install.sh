sudo -v
echo "Downloading source code..."
sudo wget https://raw.githubusercontent.com/ralphembree/SquareUp/master/square_up.py -O /usr/games/squareup
sudo chmod +x /usr/games/squareup
echo "Downloading icon..."
sudo wget https://raw.githubusercontent.com/ralphembree/SquareUp/master/square_up.png -O /usr/share/icons/hicolor/96x96/apps/squareup.png
sudo update-icon-caches /usr/share/icons/hicolor

echo "Adding menu item..."
echo "[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Icon[en_US]=squareup
Name[en_US]=Square Up
Exec=squareup
Name=Square Up
Icon=squareup
Categories=Game;" | sudo tee /usr/share/applications/squareup.desktop >/dev/null
sudo desktop-file-validate /usr/share/applications/squareup.desktop
sudo cp /usr/share/applications/squareup.desktop $HOME/.local/share/applications/squareup.desktop
sudo update-desktop-database /usr/share/applications
sudo update-desktop-database $HOME/.local/share/applications
echo -e "\033[1;34mThe application can be run from the terminal by typing 'squareup' and can also be accessed from the start menu in the games section.\033[m"

